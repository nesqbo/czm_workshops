package com.example.easyapp.repository;

import com.example.easyapp.entity.Osoba;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OsobaRepository extends JpaRepository<Osoba, Integer> {
}
